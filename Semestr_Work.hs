data Term = Var String 
          | Lam String Term 
          | App Term Term
          deriving(Eq)

instance Show Term where
    show (Var x) = x
    show (App t1 t2) = "(" ++ (show t1) ++ " " ++ (show t2)++ ")"
    show (Lam v t) = "(\\" ++ v ++ "." ++ (show t) ++ ")" 

specialVar :: [String] -> String -> Int -> String
specialVar arr x n | elem (x ++ (show n)) arr = specialVar arr x (n+1)
                       | otherwise = x ++ (show n)

-- vars change			   
changeAllVars :: [String] -> Term -> Term
changeAllVars arr (Lam x term) | elem x arr = Lam x (changeAllVars (filter (\el -> el /= x) arr) term)
                                        | otherwise = Lam x (changeAllVars arr term)
changeAllVars arr (App t1 t2) = App (changeAllVars arr t1) (changeAllVars arr t2)
changeAllVars arr (Var x) | elem x arr = Var (specialVar arr x 0)
                                     | otherwise = Var x

--substitution
subst :: [String] -> Term -> String -> Term -> Term
subst arr (Var y) x v = if x == y then (changeAllVars arr v) else (Var y)
subst arr (Lam y e) x v = if x == y then (Lam y e) else (Lam y (subst (y:arr) e x v))
subst arr (App e1 e2) x v = App (subst arr e1 x v) (subst arr e2 x v)

--computing one step
evalOne :: Term -> Term
evalOne (App (Var x) t) = (App (Var x) (evalOne t))
evalOne (App (Lam x e) v) = subst [] e x v
evalOne (App (App t1 t2) t3) = App (evalOne (App t1 t2)) t3
evalOne t = t

--computing several steps
evalAll :: Term -> Term
evalAll (App (Var x) t) = (App (Var x) (evalAll t))
evalAll (App (Lam x e) v) = evalAll (subst [] e x v)
evalAll (App (App t1 t2) t3) = evalAll (App (evalAll (App t1 t2)) t3)
evalAll t = t


y = Var "y"
x = Var "x"
xx = Lam "x" x

-- Test vars
test1 = App (Lam "x" (Lam "y" (Var "y"))) (Var "y")
test2 = App (Lam "x" (Lam "y" (Var "x"))) (Lam "z" (Var "z"))
test3 = App (Lam "x" (Lam "y" (Var "y"))) (Var "z")
test4 = App xx (App xx (Lam "z" (App xx (Var "z"))))
test5 = App (App (Lam "x" (Lam "y" (App (Var "x") (Var "y")))) (Lam "z" (Var "z"))) (Lam "u" (Lam "v" (Var "v")))
test6 = App (Lam "x" $ Lam "y" $ Var "x") (Lam "x" $ Var "y")
test7 = App (App (App (App (Lam "m" $ Lam "n" $ Lam "s" $ Lam "z" $ App (App (Var "m") (App (Var "n") (Var "s"))) (Var "z")) (Lam "s" $ Lam "z" $ App (Var "s") $ App (Var "s") $ Var "z")) (Lam "s" $ Lam "z" $ App (Var "s") $ App (Var "s") $ Var "z")) (Var "S")) (Var "Z")
